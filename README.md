shell-lqdn
=========

Ce rôle va configurer l'accès aux serveurs de LQDN. Il se charge d'installer des paquets par défaut, de configurer SSH, ainsi que l'accès des membres à ces serveurs à travers une clé SSH.

Requirements
------------

Aucune dépendances.

Role Variables
--------------

Le rôle permet d'ajouter des utilisateurices via la variable `people`, que vous pouvez éditer dans le dossier `vars`.

Dependencies
------------

N/A

Example Playbook
----------------

    - hosts: servers
      roles:
         - { role: shell-lqdn, vault_X_password: !vault | XXXXX..  }

License
-------

AGPLV3

Author Information
------------------

np@laquadrature.net
